# Overview

Start temporal workflow from Java (quarkus app).

Perform workflow in python worker

# Usage

1. Run temporal server: https://github.com/temporalio/docker-compose
2. Run quarkus app: ```cd java-initiator && ./mvnw quarkus:dev```
3. Build and python worker (see python-worker/README.md)
4. Start workflow, sending http request to quarkus app: curl localhost:8081/start
5. See workflow status in temporal-ui: http://localhost:8080

# Credits
Python code based on https://github.com/temporalio/sdk-python