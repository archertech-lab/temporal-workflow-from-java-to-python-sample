# Python worker

Worker performing tasks from greeting-task-queue

Based on [Python SDK](https://github.com/temporalio/sdk-python).

## Usage

Prerequisites:

* Python >= 3.7
* [Poetry](https://python-poetry.org)
* [Local Temporal server running](https://docs.temporal.io/application-development/foundations#run-a-development-cluster)

With this repository cloned, run the following at the root of the directory:

    poetry install

That loads all required dependencies. Then to run a sample, usually you just run it in Python. For example:

    poetry run python greeting_workflow_worker.py

