import asyncio
import logging
from dataclasses import dataclass
from datetime import timedelta

from temporalio import activity, workflow
from temporalio.client import Client
from temporalio.worker import Worker


# While we could use multiple parameters in the activity, Temporal strongly
# encourages using a single dataclass instead which can have fields added to it
# in a backwards-compatible way.
@dataclass
class ComposeGreetingInput:
    greeting: str
    name: str

@activity.defn
async def compose_greeting(input: ComposeGreetingInput) -> str:
    activity.logger.info("Running activity with parameter %s" % input)
    return f"{input.greeting}, {input.name}!"


# Basic workflow that logs and invokes an activity
@workflow.defn
class GreetingWorkflow:
    @workflow.run
    async def run(self, name: str) -> str:
        workflow.logger.info("Running workflow with parameter %s" % name)
        return await workflow.execute_activity(
            compose_greeting,
            ComposeGreetingInput("Hello", name),
            start_to_close_timeout=timedelta(seconds=10),
        )

# https://docs.temporal.io/application-development/foundations?lang=python#run-a-dev-worker
async def run_worker(stop_event: asyncio.Event):
    # Create Client connected to server at the given address
    # client = await Client.connect("127.0.0.1:7233", namespace="your-custom-namespace")
    client = await Client.connect("127.0.0.1:7233")

    # Run the worker until the event is set
    worker = Worker(
        client,
        task_queue="greeting-task-queue",
        workflows=[GreetingWorkflow],
        activities=[compose_greeting],
    )
    async with worker:
        await stop_event.wait()

if __name__ == "__main__":
    # https://stackoverflow.com/a/64757767/827704
    stop_event = asyncio.Event()
    asyncio.run(run_worker(stop_event))
