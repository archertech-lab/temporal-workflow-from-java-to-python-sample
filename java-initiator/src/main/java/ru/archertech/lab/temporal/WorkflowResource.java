package ru.archertech.lab.temporal;

import io.temporal.api.common.v1.WorkflowExecution;
import io.temporal.client.WorkflowClient;
import io.temporal.client.WorkflowOptions;
import io.temporal.client.WorkflowStub;
import io.temporal.serviceclient.WorkflowServiceStubs;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/start")
public class WorkflowResource {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String startWorkflowExecution() {
        // WorkflowServiceStubs is a gRPC stubs wrapper that talks to the local Docker instance of the Temporal server.
        WorkflowServiceStubs service = WorkflowServiceStubs.newLocalServiceStubs();
        WorkflowOptions options = WorkflowOptions.newBuilder()
                .setTaskQueue("greeting-task-queue")
                // A WorkflowId prevents it from having duplicate instances, remove it to duplicate.
//                .setWorkflowId("say-hello-workflow")
                .build();
        // WorkflowClient can be used to start, signal, query, cancel, and terminate Workflows.
        WorkflowClient client = WorkflowClient.newInstance(service);
        // WorkflowStubs enable calls to methods as if the Workflow object is local, but actually perform an RPC.
        // https://docs.temporal.io/application-development/foundations?lang=java#start-workflow-execution
        WorkflowStub untyped = client.newUntypedWorkflowStub("GreetingWorkflow", options);

        String name = "TestName";
        // Asynchronous execution. This process will exit after making this call.
        WorkflowExecution we = untyped.start(name);
        return String.format("Started workflow: WorkflowID: %s RunID: %s", we.getWorkflowId(), we.getRunId());
    }
}